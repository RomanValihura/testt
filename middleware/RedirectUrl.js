var HttpError = require('../error/HttpError');
var Url       = require('../model/Url').Url;

module.exports = function (req, res, next) {
   var urlId            = req.params.urlId;
   var validateIdRegExp = new RegExp('^[a-zA-Z0-9]+$');

   if (validateIdRegExp.test(urlId) === false) {
      return next(new HttpError(404));
   }

   Url.findOne({shortCode: urlId}, function (err, doc) {
      if (doc) {
         res.redirect(301, doc.long);
      } else {
         return next(new HttpError(404));
      }
   })
}