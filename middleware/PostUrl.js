var HttpError = require('../error/HttpError');
var Url       = require('../model/Url').Url;

const urlRegExp = /^((http[s]?):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?$/;

module.exports = function (req, resp, next) {
   var hostname = req.protocol + '://' + req.get('host');

   var sendResponse = function (shortCode) {
      resp.send({
         short: hostname + '/' + shortCode
      });
   }

   if (!urlRegExp.test(req.body.url)) {
      throw new HttpError(400, 'URL isn\'t valid');
   }

   Url.findOne({long: req.body.url}, function (err, doc) {
      if (err)
         return next(err);

      if (!doc) {
         var url = new Url({
            long: req.body.url,
         });

         url.save(function (err, url) {
            if (err) {
               return next(new HttpError(500, 'Save error'));
            }

            sendResponse(url.shortCode);
         })
      } else {
         sendResponse(doc.shortCode);
      }
   })
}