var HttpError = require('../error/HttpError');
var path = require('path');

module.exports = function (err, req, res, next) {
   if (err instanceof HttpError) {
      res.status(err.code);

      switch (err.code) {
         case 404:
            res.sendFile(path.resolve('public/404.html'));

            break;

         default:
            if (req.accepts('json')) {
               res.send({
                  error: err.message
               });

               return;
            }
      }
   }

   if(err)
      res.sendFile(path.resolve('public/unknown-error.html'));
};