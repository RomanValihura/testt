var util = require('util');

function HttpError(statusCode, description) {
   Error.apply(this, arguments);
   Error.captureStackTrace(this, HttpError);

   this.code    = statusCode;
   this.message = description || 'Something wend wrong';
}

util.inherits(HttpError, Error);

HttpError.prototype.name = 'HttpError';

module.exports = HttpError;