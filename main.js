var express    = require('express');
var bodyParser = require('body-parser');
var app        = express();

app.use(bodyParser.urlencoded({extended: true}));

app.use(bodyParser.json());

app.use('/media', express.static(__dirname + '/public/media'));

require('./libs/router')(app);

app.use(require('./middleware/ErrorHandler'));

var server = app.listen(8080, function () {

   var host = server.address().address
   var port = server.address().port

   console.log("Test app listening at http://%s:%s", host, port)

})