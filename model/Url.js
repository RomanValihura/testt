var mongoose      = require('../libs/mongoose');
var autoIncrement = require('mongoose-auto-increment');
var Schema        = mongoose.Schema;

var urlSchema = new Schema({
   long: {
      type: String,
      unique: true,
      required: true,
   },
   shortCode: {
      type: Number,
      unique: true,
   },
   created: {
      type: Date,
      default: Date.now,
   },
})

var url = mongoose.model('url', urlSchema);

urlSchema.plugin(autoIncrement.plugin, {model: 'url', field: 'shortCode'});

module.exports.Url = url;