var mongoose      = require('mongoose');
var config        = require('config');
var autoIncrement = require('mongoose-auto-increment');

mongoose.Promise = global.Promise;
mongoose.connect(config.get('mongoose.uri'));

autoIncrement.initialize(mongoose.connection);

module.exports = mongoose;