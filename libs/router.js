var path = require('path');

module.exports = function (app) {
   app.get('/', function (req, res) {
      res.sendFile(path.resolve('public/index.html'));
   });

   app.get('/:urlId', require('../middleware/RedirectUrl'));

   app.post('/short', require('../middleware/PostUrl'));
}